<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function project()
    {
        $this->belongsTo(Project::class);
    }

    public function group_user()
    {
        $this->hasMany(GroupUser::class);
    }

    public function group_project()
    {
        $this->hasMany(GroupProject::class);
    }
}
