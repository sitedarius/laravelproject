<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name', 'finished', 'project_id'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
