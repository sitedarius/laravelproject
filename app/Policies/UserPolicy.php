<?php

namespace App\Policies;

use App\Project;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function isAdmin(User $user)
    {
        if($user->is_admin == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can view any projects.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function viewAny(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }

    /**
     * Determine whether the user can view the project.
     *
     * @param \App\User $user
     * @param \App\Project $project
     * @return mixed
     */
    public function view(User $user, Project $project)
    {
        if ($user->id === $project->user_id) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can create projects.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (Auth::user() === true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine whether the user can update the project.
     *
     * @param \App\User $user
     * @param \App\Project $project
     * @return mixed
     */
    public function update(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }

    /**
     * Determine whether the user can delete the project.
     *
     * @param \App\User $user
     * @param \App\Project $project
     * @return mixed
     */
    public function edit(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }

    /**
     * Determine whether the user can delete the project.
     *
     * @param \App\User $user
     * @param \App\Project $project
     * @return mixed
     */
    public function delete(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }

    /**
     * Determine whether the user can restore the project.
     *
     * @param \App\User $user
     * @param \App\Project $project
     * @return mixed
     */
    public function restore(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }

    /**
     * Determine whether the user can permanently delete the project.
     *
     * @param \App\User $user
     * @param \App\Project $project
     * @return mixed
     */
    public function forceDelete(User $user, Project $project)
    {
        return $user->id === $project->user_id;
    }
}
