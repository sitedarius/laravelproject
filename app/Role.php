<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function user()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function role_project()
    {
        return $this->hasMany(RoleProject::class);
    }
}
