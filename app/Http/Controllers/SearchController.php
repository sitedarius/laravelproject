<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    public function getSearch()
    {
        return back();
    }
    public function search(Request $request)
    {

        $projects = Project::where('name', 'LIKE', '%' . $request->q . '%')->where('user_id', 1)->orwhere([['description_short', 'LIKE', '%' . $request->q . '%'], ['user_id', 1]])->get();

        $search = $request->q;

        return view('search', compact('projects', 'search'));
    }
}
