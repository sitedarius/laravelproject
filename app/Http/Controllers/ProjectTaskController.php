<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;

class ProjectTaskController extends Controller
{
    public function store(Project $project)
    {
        request()->validate(['name' => 'required']);

        $task = new Task();
        $task->name = request('name');
        $task->project_id = $project->id;

        $task->save();

        return back();
    }

    public function update(Task $task)
    {
        $task->update([
            'finished' => request()->has('finished')
        ]);

        return back();
    }
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return back();
    }

}
