<?php

namespace App\Http\Controllers;

use App\Group;
use App\GroupProject;
use App\Project;
use Illuminate\Http\Request;

class GroupsController extends Controller
{
    public function store(Request $request, $id)
    {
        $data = $request->validate([
            'group_id' => 'required',
        ]);
        $groupproject = new GroupProject();
        $groupproject->group_id = $data['group_id'];
        $groupproject->project_id = $id;
        $groupproject->save();

        return back();
    }

    public function edit($id)
    {
        return view('groups.edit');
    }

    public function update(Request $request, $id)
    {
        $groupproject = GroupProject::findOrFail($id);

        $groupproject->update();
        $groupproject->group_id = request('group_id');
        $groupproject->project_id = $id;
        $groupproject->save();

        return back();
    }
}
