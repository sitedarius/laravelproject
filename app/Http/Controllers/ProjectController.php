<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function index(User $user)
    {
        $user_id = Auth::user()->id;

        $projects = Project::where('user_id', $user_id)->get();

        return view('projects.index', compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'name' => 'required',
            'description_short' => 'required',
            'description_long' => 'required',
        ]);

        $imageName = time() . '.' . request()->cover_image->getClientOriginalName();

        $project = new Project();
        $project->name = request('name');
        $project->description_short = request('description_short');
        $project->description_long = request('description_long');
        $project->user_id = Auth::user()->id;
        $project->cover_image = $imageName;

        $project->save();

        request()->cover_image->move(public_path('storage/projects/' . $project->id), $imageName);

        return redirect(route('home'));
    }

    public function show($id)
    {
        $user = Auth::user();
        $user_id = Auth::user()->id;

        $project = Project::findOrFail($id);

        $this->authorize('view', $project);
        return view('projects.show', compact('project', 'user'));
    }

    public function edit($id)
    {
        $user = Auth::user();

        $project = Project::findOrFail($id);

        if ($user->can('edit', $project)) {
            return view('projects.edit', compact('project'));
        } else {
            return view('unauthorized');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        request()->validate([
            'name' => 'required',
            'description_short' => 'required',
            'description_long' => 'required'
        ]);

        $projects = Project::findOrFail($id);

        $projects->name = request('name');
        $projects->description_short = request('description_short');
        $projects->description_long = request('description_long');

        if ($request->cover_image) {
            $logoName = time() . '.' . request()->cover_image->getClientOriginalName();
            request()->cover_image->move(public_path('storage/projects/' . $projects->id), $logoName);
            $projects->cover_image = $logoName;
        }

        $projects->save();

        return redirect(route('show.project', $id));
    }

    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();

        return redirect(route('home'));
    }
}
