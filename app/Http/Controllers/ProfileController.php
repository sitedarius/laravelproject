<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $projects = Project::where('user_id', $user_id)->get();

        $user = User::findOrFail($id);
        return view('profile.show',  compact('user', 'projects'));
    }

    public function update($id)
    {
        request()->validate([
            'email' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'website' => 'required',
            'password' => 'required', 'string', 'min:8', 'confirmed',
        ]);

        $user = User::findOrFail($id);

        $user->name = request('name');
        $user->email = request('email');
        $user->phone = request('phone');
        $user->website = request('website');
        $user->password = Hash::make('password');

        $user->save();

        return back();
    }
}
