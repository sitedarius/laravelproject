<?php

namespace App\Http\Controllers;

use App\News;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        $posts = News::all();

        $user = Auth::user();
        $this->authorize('isAdmin', $user);

        return view('admin.index', compact('posts'));
    }

    public function projects()
    {
        $projects = Project::all();

        $user = Auth::user();
        $this->authorize('isAdmin', $user);

        return view('admin.projects', compact('projects'));
    }

    public function users()
    {
        $users = User::all();

        $user = Auth::user();
        $this->authorize('isAdmin', $user);

        return view('admin.users', compact('users'));
    }
}
