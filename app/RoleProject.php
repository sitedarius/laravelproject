<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleProject extends Model
{
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id','id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
}
