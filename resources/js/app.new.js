jQuery(document).ready(function() {

    $('textarea').keyup(function() {
        var box = $(this).val();
        var main = box.length * 100;
        var value = (main / 100);
        var count = 0 + box.length;
        var reverse_count = 100 - box.length;

        if(box.length >= 0){
            $('.progress-bar').css('width', count + '%');
            $('.current-value').text(count + '%');
            $('.count').text(reverse_count);

            if (count >= 50 && count < 85){
                $('.progress-bar').removeClass('progress-bar-danger').addClass('progress-bar-warning');
            }
            if (count > 85){
                $('.progress-bar').removeClass('progress-bar-warning').addClass('progress-bar-danger');
            }
            if(count >= 0 && count < 50){
                $('.progress-bar').removeClass('progress-bar-danger');
                $('.progress-bar').removeClass('progress-bar-warning');
                $('.progress-bar').addClass('progress-bar-success')
            }

        }
        return false;
    });
});