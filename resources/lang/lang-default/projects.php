<?php

return [
//    Projecten
    'edit' => 'Bewerken',
    'status_empty' => 'Privé',
    'status' => 'Gedeeld',
    'created_at' => 'Aangemaakt op:',
    'image' => 'Foto:',

    'create' => 'Maak een project aan',
    'edit_page' => 'Bewerk een project:',
    'name' => 'Naam:',
    'short_description' => 'Korte beschrijving:',
    'long_description' => 'Tekst:',
    'upload_image' => 'Upload een foto:',
    'save' => 'Opslaan',
];