<?php

return [
//    Algemeen
    'news' => 'Nieuws',
    'projects' => 'Projecten',
    'sure' => 'Weet u het zeker?',
    'search' => 'Zoeken...',
    'search_noresult' => 'Er zijn geen resultaten voor:',
    'search_result' => 'Resultaten voor:',
    'welcome' => 'Welkom',
    'admin' => 'Admin-pagina',
    'profile' => 'Profiel',
    'empty' => 'Hier staat nog niks!',
];