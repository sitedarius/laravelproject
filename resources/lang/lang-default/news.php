<?php

return [
//    Nieuws
    'read_more' => 'Lees Meer...',
    'comments' => 'Reacties:',
    'author' => 'Auteur:',
    'by' => 'Door:',
    'updated' => 'Laatste update:',
    'no_comments' => 'Er zijn nog geen reacties...',

    'name' => 'Naam:',
    'short_description' => 'Korte beschrijving:',
    'long_description' => 'Tekst:',
    'upload_image' => 'Upload een foto:',
    'save' => 'Opslaan',
    'edit' => 'Bewerken',
];