<?php

return [
//    Algemeen
    'news' => 'Nieuws',
    'projects' => 'Projecten',
    'sure' => 'Weet u het zeker?',
    'search' => 'Zoeken...',
    'search_noresult' => 'Er zijn geen resultaten voor:',
    'search_result' => 'Resultaten voor:',
    'welcome' => 'Welkom',
    'admin' => 'Admin-pagina',
    'profile' => 'Profiel',
    'empty' => 'Hier staat nog niks!',

//    Nieuws
    'read_more' => 'Lees Meer...',
    'comments' => 'Reacties:',
    'author' => 'Auteur:',
    'by' => 'Door:',
    'updated' => 'Laatste update:',
    'no_comments' => 'Er zijn nog geen reacties...',

//    Projecten
    'edit' => 'Bewerken',
    'tasks' => 'Taken:',
    'tasks_empty' => 'Je hebt geen taken...',
    'status_empty' => 'Privé',
    'status' => 'Gedeeld',
    'created_at' => 'Aangemaakt op:',
    'image' => 'Foto:',
    'edit_page' => 'Bewerk een project:',

    'name' => 'Naam:',
    'short_description' => 'Korte beschrijving:',
    'long_description' => 'Tekst:',
    'upload_image' => 'Upload een foto:',
    'save' => 'Opslaan',

];
?>