<?php

return [
//    Algemeen
    'news' => '',
    'projects' => '',
    'sure' => '',
    'search' => '',
    'search_noresult' => '',
    'search_result' => '',
    'welcome' => '',
    'admin' => '',
    'profile' => '',
    'empty' => '',
    'language' => '',
    'required' => '',

//    Forms
    'name' => ':',
    'short_description' => ':',
    'long_description' => ':',
    'upload_image' => ':',
    'text' => ':',
    'intro' => ':',
    'title' => ':',
    'save' => '',
];