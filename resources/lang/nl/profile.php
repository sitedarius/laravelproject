<?php

return [
//    Profiel
    'title' => 'Bewerk je profiel:',
    'name' => 'Naam:',
    'phone' => 'Telefoon:',
    'password' => 'Wachtwoord:',
    'repeat' => 'Herhaal wachtwoord:',
    'groups' => 'Groepen:',
    'recent_project' => 'Meest recente project:',
    'joined' => 'Aangemeld op:',
];