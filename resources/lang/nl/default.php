<?php

return [
//    Algemeen
    'news' => 'Nieuws',
    'projects' => 'Projecten',
    'sure' => 'Weet u het zeker?',
    'search' => 'Zoeken...',
    'search_noresult' => 'Er zijn geen resultaten voor:',
    'search_result' => 'Resultaten voor:',
    'welcome' => 'Welkom',
    'admin' => 'Admin-pagina',
    'profile' => 'Profiel',
    'empty' => 'Hier staat nog niks!',
    'language' => 'Taal',
    'required' => 'Verplicht',
    'contacts' => 'Contacten',
    'files' => 'Bestanden',
    'dashboard' => 'Dashboard',
    'agenda' => 'Agenda',
    'logout' => 'Uitloggen',

    'name' => 'Naam:',
    'short_description' => 'Korte beschrijving:',
    'long_description' => 'Tekst:',
    'upload_image' => 'Upload een foto:',
    'text' => 'Tekst:',
    'intro' => 'Intro:',
    'title' => 'Titel:',
    'save' => 'Opslaan',
];