<?php

return [
//    Profiel
    'title' => 'Edit your profile:',
    'name' => 'Name:',
    'phone' => 'Phone:',
    'password' => 'Password:',
    'repeat' => 'Repeat password:',
    'groups' => 'Groups:',
    'recent_project' => 'Most recent project:',
    'joined' => 'Joined on:',
];
