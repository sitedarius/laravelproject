<?php

return [
//    Standards
    'news' => 'News',
    'projects' => 'Projects',
    'sure' => 'Are you sure?',
    'search' => 'Search...',
    'search_noresult' => 'There are no results for:',
    'search_result' => 'Results for:',
    'welcome' => 'Welcome',
    'admin' => 'Admin-page',
    'profile' => 'Profile',
    'empty' => 'Nothing here yet!',
    'language' => 'Language',
    'required' => 'Required',
    'contacts' => 'Contacts',
    'files' => 'Files',
    'dashboard' => 'Dashboard',
    'agenda' => 'Agenda',
    'logout' => 'Sign Out',

    'name' => 'Name:',
    'short_description' => 'Short description:',
    'long_description' => 'Text:',
    'upload_image' => 'Upload a cover image:',
    'save' => 'Save',
    'text' => 'Text:',
    'intro' => 'Intro:',
    'title' => 'Title:',
];