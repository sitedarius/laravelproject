@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3>Create a group:</h3>
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        </div>

                        <button class="btn btn-primary" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('store.group', [$project->id, ]) }}">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlSelect1">Select a group:</label>
            <select class="form-control" id="exampleFormControlSelect1" name="group_id">
                <option readonly>Select one..</option>
                @foreach($groupsusers as $groupuser)
                    <option value="{{ $groupuser->group->id }}"
                            name="group_id">{{ $groupuser->group->name }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </form>

@endsection
