<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/app.new.css">

    <link rel="icon" type="image/png" href="/storage/logo/favicon.png">

    <script src="https://kit.fontawesome.com/6f85a7f239.js" crossorigin="anonymous"></script>

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark navbar-top d-none d-md-block">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">CRUD</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            {{--            <img src="/storage/logo/favicon.png" style="width: 20%;">--}}
            <form class="form-inline my-2 my-lg-0 mr-auto ml-auto" method="POST" action="{{ route('search') }}">
                @csrf
                <div class="p-1 bg-light rounded rounded-pill shadow-sm">
                    <div class="input-group">
                        <input type="search" placeholder="{{ __('default.search') }}" aria-describedby="button-addon1"
                               class="form-control border-0 bg-light" name="q" id="q"
                               style="border-radius: 25px 0 0 25px;">
                        <div class="input-group-append">
                            <button id="button-addon1" type="submit" class="btn btn-link"><i
                                    class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </form>

            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#exampleModal"
                           href="{{ route('login') }}">{{ __('auth.login') }}</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item"
                               href="{{ route('show.profile', [Auth::user()->id]) }}">{{ __('default.profile') }}</a>
                            @if(Auth::check() && Auth::user()->is_admin == true)
                                <a class="dropdown-item"
                                   href="{{ route('admin.index') }}">{{ __('default.admin') }}</a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('auth.logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>

        </div>
    </div>
</nav>

{{--Navbar MOBILE--}}
<nav class="navbar navbar-expand-lg navbar-dark navbar-top d-block d-md-none">
    <div class="container">
        <button class="navbar-toggler border-0" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand mx-auto" href="{{ route('home') }}">CRUD</a>
        {{--        <img src="/storage/logo/favicon.png" style="width: 20%;">--}}
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        </div>
        <ul class="navbar-nav ml-auto">
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i></a>
                </li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <i class="fas fa-user"></i> <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item"
                           href="{{ route('show.profile', [Auth::user()->id]) }}">{{ __('messages.profile') }}</a>
                        @if(Auth::check() && Auth::user()->is_admin == true)
                            <a class="dropdown-item" href="{{ route('admin.index') }}">{{ __('messages.admin') }}</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Log out') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('news') }}">{{ __('default.news') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('projects') }}">{{ __('default.projects') }}</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('default.language') }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('locale/en') }}"><img
                                src="https://img.icons8.com/color/25/000000/usa.png"> - USA</a>
                        <a class="dropdown-item" href="{{ url('locale/nl') }}"><img
                                src="https://img.icons8.com/color/25/000000/netherlands.png"> - Nederland</a>
                        <a class="dropdown-item disabled" href="{{ url('locale/de') }}"><img
                                src="https://img.icons8.com/color/25/000000/germany.png"> - Deutschland</a>
                        <a class="dropdown-item disabled" href="{{ url('locale/fr') }}"><img
                                src="https://img.icons8.com/color/25/000000/france.png"> - France</a>
                    </div>
                </li>
            </ul>


        </div>
    </div>
</nav>

{{--Modal for loggin in--}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Login') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container my-5">
    @yield('content')
</div>

</body>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-1.5.js"></script>
<script>
    $('.delete-user').click(function (e) {
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('{{ __('messages.sure') }}')) {
            // Post the form
            $(e.target).closest('form').submit() // Post the surrounding form
        }
    });

    function countChar(val) {
        var len = val.value.length;
        if (len >= 100) {
            val.value = val.value.substring(0, 100);
        } else {
            $('#charNum').text(100 - len);
        }
    };

    function countChar1(val) {
        var len = val.value.length;
        if (len >= 37) {
            val.value = val.value.substring(0, 37);
        } else {
            $('#charNum1').text(37 - len);
        }
    };
</script>

</html>
