<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/app.new.css">

    <link rel="icon" type="image/png" href="/storage/logo/favicon.png">

    <script src="https://kit.fontawesome.com/6f85a7f239.js" crossorigin="anonymous"></script>

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">CRUD</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ __('default.language') }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('locale/en') }}"><img
                                    src="https://img.icons8.com/color/25/000000/usa.png"> - USA</a>
                        <a class="dropdown-item" href="{{ url('locale/nl') }}"><img
                                    src="https://img.icons8.com/color/25/000000/netherlands.png"> - Nederland</a>
                        <a class="dropdown-item disabled" href="{{ url('locale/de') }}"><img
                                    src="https://img.icons8.com/color/25/000000/germany.png"> - Deutschland</a>
                        <a class="dropdown-item disabled" href="{{ url('locale/fr') }}"><img
                                    src="https://img.icons8.com/color/25/000000/france.png"> - France</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="container-fluid">
    <div class="row">
        <div class="col-2 px-0">
            <div class="sidebar px-3">
                <br>
                <form class="form-inline my-2 my-lg-0 mr-auto ml-auto" method="POST" action="{{ route('search') }}">
                    @csrf
                    <div class="p-1 bg-light rounded rounded-pill shadow-sm">
                        <div class="input-group">
                            <input type="search" placeholder="{{ __('default.search') }}"
                                   aria-describedby="button-addon1"
                                   class="form-control border-0 bg-light" name="q" id="q"
                                   style="border-radius: 25px 0 0 25px;">
                            <div class="input-group-append">
                                <button id="button-addon1" type="submit" class="btn btn-link"><i
                                            class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                @guest
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email"
                                   class="col-12 col-form-label font-weight-bold">{{ __('E-Mail Address') }}</label>
                            <div class="col-12">
                                <input id="email" type="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ old('email') }}" required autocomplete="email"
                                       autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password"
                                   class="col-12 col-form-label font-weight-bold">{{ __('Password') }}</label>
                            <div class="col-12">
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror"
                                       name="password"
                                       required
                                       autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember"
                                           id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>
                        <br>
                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                        @if (Route::has('register'))
                            <a class="btn btn-link" href="{{ route('register') }}">
                                {{ __('Create a account') }}
                            </a>
                        @endif
                    </form>
                @else
                    <div class="text-center justify-content-center">
                        <img src="http://public.crunchbase.com/t_api_images/v1443020871/pbjzpjnyqt0nod1gx2gv.png"
                             class="w-25 rounded-circle mb-2">
                        <p class="mb-0 font-weight-bold">{{ Auth::user()->name }} </p>
                        <p>The Cool Frog</p>
                    </div>
                    <hr>
                    <ul class="my-5">
                        <li class="my-2"><a href="{{ route('home') }}" class="sidebar-link"><i
                                        class="fas fa-columns"></i> - {{ __('default.dashboard') }}</a>
                        </li>
                        <li class="my-2"><a href="{{ route('projects') }}" class="sidebar-link"><i
                                        class="fas fa-tasks"></i> - {{ __('default.projects') }}</a>
                        </li>
                        {{--                        <li class="my-2"><a href="" class="sidebar-link"><i class="far fa-folder-open"></i> - {{ __('default.files') }}</a>--}}
                        {{--                        </li>--}}
                        <li class="my-2"><a href="{{ route('news') }}" class="sidebar-link"><i
                                        class="far fa-newspaper"></i> - {{ __('default.news') }}</a>
                        </li>
                        {{--                        <li class="my-2"><a href="" class="sidebar-link"><i class="far fa-calendar-alt"></i> ---}}
                        {{--                                {{ __('default.agenda') }}</a></li>--}}
                        {{--                        <li class="my-2"><a href="" class="sidebar-link"><i class="far fa-address-book"></i> - {{ __('default.contacts') }}</a>--}}
                        {{--                        </li>--}}
                        <li class="my-2"><a href="{{ route('show.profile', [Auth::user()->id]) }}" class="sidebar-link"><i
                                        class="fas fa-user"></i> - {{ __('default.profile') }}</a></li>
                        <li class="my-2"><a class="sidebar-link text-danger" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                        class="fas fa-sign-out-alt"></i> - {{ __('default.logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                    <hr>
                    {{--                    <div class="bottom-bar">--}}
                    {{--                        <div class="row">--}}
                    {{--                            <div class="col-4"><i class="far fa-question-circle"></i></div>--}}
                    {{--                            <div class="col-4"><i class="fas fa-cog"></i></div>--}}
                    {{--                            <div class="col-4"><i class="fas fa-sign-out-alt"></i></div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                @endguest
            </div>
        </div>

        <div class="col-md-10">
            <div class="container-fluid my-5">
                @yield('content')
            </div>
        </div>

    </div>
</div>

</body>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-1.5.js"></script>
<script>
    $('.delete-user').click(function (e) {
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('{{ __('messages.sure') }}')) {
            // Post the form
            $(e.target).closest('form').submit() // Post the surrounding form
        }
    });

    function countChar(val) {
        var len = val.value.length;
        if (len >= 100) {
            val.value = val.value.substring(0, 100);
        } else {
            $('#charNum').text(100 - len);
        }
    };

    function countChar1(val) {
        var len = val.value.length;
        if (len >= 37) {
            val.value = val.value.substring(0, 37);
        } else {
            $('#charNum1').text(37 - len);
        }
    };

    $('.dropdown-radio').find('input').change(function () {
        var dropdown = $(this).closest('.dropdown');
        var radioname = $(this).attr('name');
        var checked = 'input[name=' + radioname + ']:checked';

        //update the text
        var checkedtext = $(checked).closest('.dropdown-radio').text();
        dropdown.find('button').text(checkedtext);

        //retrieve the checked value, if needed in page
        var thisvalue = dropdown.find(checked).val();
        alert(thisvalue);
    });
</script>

</html>
