@extends('layouts.app')

@section('title', 'Search results')

@section('content')

    <div class="row">
        <div class="col-md-12"><a href="{{ redirect()->getUrlGenerator()->previous() }}"><i
                        class="fas fa-arrow-left"></i></a></div>
        <div class="col-md-12">
            @if (count($projects))
                <h3>{{ __('default.search_result') }}{{ $search }}</h3>
                <ul class="list-unstyled">
                    <div class="row">
                        @foreach($projects as $project)
                            <div class="col-md-4 mt-2">
                                <a href="{{ route('show.project', [$project->id]) }}">
                                    <div class="card">
                                        <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}"
                                             class="card-img-top" alt="{{ $project->cover_image }}">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $project->name }}</h5>
                                            <hr>
                                            <p class="card-text">{{ $project->description_short }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </ul>
            @else
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10 justify-content-center text-center mt-5">
                        <img src="/storage/logo/favicon.png" class="w-50">
                        <h3 class="mt-5">{{ __('default.search_noresult') }} {{ $search }}</h3>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            @endif


        </div>

    </div>

@endsection
