@extends('layouts.app')

@section('title', 'edit.blade.php')

@section('content')

    <div class="row">
        <div class="col-md-12"><a href="{{ route('show.project', [$project->id]) }}"><i
                        class="fas fa-arrow-left"></i></a></div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-11 col-11">
                            <h3>{{ __('projects.edit_page') }}</h3>
                        </div>
                        <div class="col-md-1 col-1">
                            <form method="POST" action="{{ route('delete.project', [$project->id]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-link p-0 delete-user"><i
                                            class="fas fa-trash-alt"></i></button>
                            </form>
                        </div>
                        <div class="col-12">
                            <hr>
                            <form method="POST" action="{{ route('update.project', [$project->id]) }}"
                                  enctype="multipart/form-data">
                                @method('PATCH')
                                @csrf
                                <div class="form-group">
                                    <label for="project_name">{{ __('default.name') }}*</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                           id="project_name" name="name" value="{{ $project->name }}" maxlength="37"
                                           onkeyup="countChar1(this)">
                                    <small>{{ $errors->first('name') }}
                                        <div id="charNum1"></div>
                                    </small>
                                </div>
                                <div class="form-group">
                                    <label for="description_short">{{ __('default.short_description') }}*</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('description_short') ? 'is-invalid' : '' }}"
                                           id="description_short" name="description_short"
                                           value="{{ $project->description_short }}" maxlength="100"
                                           onkeyup="countChar(this)">
                                    <small>{{ $errors->first('description_short') }}
                                        <div id="charNum"></div>
                                    </small>
                                </div>
                                <div class="form-group">
                                    <label for="cover_image">{{ __('default.upload_image') }}</label>
                                    <input type="file"
                                           class="form-control-file {{ $errors->has('cover_image') ? 'is-invalid' : '' }}"
                                           id="cover_image" name="cover_image" value="{{ $project->cover_image }}">
                                    <small>{{ $errors->first('cover_image') }}</small>
                                </div>
                                @if($project->cover_image)
                                    <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}"
                                         class="w-50 mb-4" alt="{{ $project->cover_image }}">
                                @endif
                                <textarea class="description {{ $errors->has('description_long') ? 'is-invalid' : '' }}"
                                          name="description_long">{{ $project->description_long ? $project->description_long : old('description_long') }}</textarea>
                                <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                                <script>
                                    tinymce.init({
                                        selector: 'textarea.description',
                                        height: 500
                                    });
                                </script>
                                <small>{{ $errors->first('description_long') }}</small>
                                <button type="submit" class="btn btn-primary mt-2">{{ __('default.save') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection