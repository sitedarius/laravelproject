@extends('layouts.app')

@section('title', 'show.blade.php')

@section('content')

    <div class="row">
        <div class="col-md-12"><a href="{{ redirect()->getUrlGenerator()->previous() }}"><i class="fas fa-arrow-left"></i></a></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-10 col-9">
                            <h4 class="mb-0">{{ $project->name }}</h4>
                        </div>
                        <div class="col-md-2 col-3 text-left">
                            <a class="btn btn-link" href="{{ route('edit.project', [$project->id]) }}">
                                <small>{{ __('projects.edit') }}</small>
                            </a>
                        </div>
                        <div class="col-12">
                            <hr>
                            <p>{!! $project->description_long !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--Right Side--}}
        <div class="col-md-4 d-sm-none d-md-block">
            <div class="card sticky-top">
                <div class="card-body pb-3">
                    <div class="row">
                        <div class="col-10">
                            <h4 class="mb-0">{{ $project->user->name }}</h4>
                        </div>
                        <div class="col-2">
                            <a href="{{ route('edit.group', [$project->id]) }}"><i class="fas fa-share"></i></a>
                        </div>
                        <div class="col-12">
                            <hr>
                            <ul>
                                <li>{{ $project->user->email }}</li>
                                <li>{{ $project->user->phone }}</li>
                                <li><a href="http://{{ $project->user->website }}"
                                       target="_blank">{{ $project->user->website }}</a></li>
                            </ul>
                            <hr>
                        </div>
                        <div class="col-10">
                            <p><strong>{{ __('tasks.tasks') }}</strong></p>
                        </div>
                        <div class="col-2">
                            <button type="button" class="btn btn-link p-0" data-toggle="modal"
                                    data-target="#tasksModalScrollable"><i class="fas fa-plus"></i></button>
                        </div>
                        @if ($project->tasks->count())
                            @foreach ($project->tasks as $task)
                                <div class="col-md-12">
                                    <form method="POST" action="{{ route('update.task', [$task->id]) }}">
                                        @method('PATCH')
                                        @csrf
                                        <label class="checkbox {{ $task->finished ? 'is-complete' : '' }}"
                                               for="finished">
                                            <input type="checkbox" name="finished"
                                                   onChange="this.form.submit()" {{ $task->finished ? 'checked' : '' }}>
                                            {{ $task->name }}
                                        </label>
                                    </form>
                                </div>
                            @endforeach
                        @else
                            <div class="col-12 text-center">
                                <p class="mb-0"><i class="fas fa-exclamation-triangle"></i> - {{ __('tasks.tasks_empty') }}
                                </p>
                            </div>
                        @endif
                        <div class="col-12">
                            <hr>
                            @if($project->shared = 0)
                                <p><strong>Status:</strong> {{ __('projects.status_empty') }} <i class="fas fa-lock" style="color: green;"></i>
                                </p>
                            @else
                                <p><strong>Status:</strong> {{ __('projects.status') }} <i class="fas fa-lock-open"
                                                                      style="color: green;"></i></p>
                            @endif
                            <hr>
                            <small><strong>{{ __('projects.created_at') }}</strong> {{ Carbon\Carbon::parse($project->created_at)->format('j F Y h:m') }}
                            </small>
                            <hr>
                            <p class="mb-0"><strong>{{ __('projects.image') }}</strong></p>
                        </div>
                    </div>
                </div>
                <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}" class="card-img-bottom"
                     alt="{{ $project->cover_image }}">
            </div>
        </div>
    </div>

    {{--Modal Tasks--}}
    <div class="modal fade" id="tasksModalScrollable" tabindex="-1" role="dialog"
         aria-labelledby="tasksModalScrollableTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tasksModalScrollableTitle">{{ __('tasks.tasks') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('store.task', [$project->id]) }}">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('tasks.add_task') }}</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp" name="name" required>
                        </div>
                        @if ($project->tasks->count())
                            <button type="submit" class="btn btn-primary">{{ __('default.save') }}</button>
                        @else
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">{{ __('default.save') }}</button>
                            </div>
                        @endif
                    </form>
                    @if ($project->tasks->count())
                        <hr>
                        <h4>{{ __('tasks.all_task') }}</h4>
                        @foreach ($project->tasks as $task)
                            <ul class="row">
                                <li class="col-10">{{ $task->name }}</li>
                                <div class="col-2">
                                    <form method="POST" action="{{ route('delete.task', [$task->id]) }}">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-link py-0 delete-user"><i
                                                    class="fas fa-trash-alt"></i></button>
                                    </form>
                                </div>
                            </ul>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
