@extends('layouts.app')

@section('title', 'create.blade.php')

@section('content')

<div class="row">
    <div class="col-md-12"><a href="{{ route('home') }}"><i class="fas fa-arrow-left"></i></a></div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h3>{{ __('projects.create') }}</h3>
                <hr>
                <form method="POST" action="{{ route('store.project') }}">
                    @csrf
                    <div class="form-group">
                        <label for="project_name">{{ __('default.name') }}*</label>
                        <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="project_name" name="name">
                        <small>{{ $errors->first('name') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="description_short">{{ __('default.short_description') }}*</label>
                        <input type="text" class="form-control {{ $errors->has('description_short') ? 'is-invalid' : '' }}" id="description_short" name="description_short">
                        <small>{{ $errors->first('description_short') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="cover_image">{{ __('default.upload_image') }}</label>
                        <input type="file" class="form-control-file {{ $errors->has('cover_image') ? 'is-invalid' : '' }}" id="cover_image" name="cover_image">
                        <small>{{ $errors->first('cover_image') }}</small>
                    </div>
                    <textarea class="description {{ $errors->has('description_long') ? 'is-invalid' : '' }}" name="description_long"></textarea>
                    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                    <script>
                        tinymce.init({
                            selector:'textarea.description',
                            height: 500
                        });
                    </script>
                    <small>{{ $errors->first('description_long') }}</small>
                    <button type="submit" class="btn btn-primary mt-2">{{ __('default.save') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection