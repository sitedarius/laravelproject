@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-7 login-image d-none d-sm-block">
            <div class="login-wrapper">
                {{--                <img src="/storage/logo/logo.png">--}}
            </div>
        </div>
        <div class="col-md-5">
            <div class="login-card">
                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
