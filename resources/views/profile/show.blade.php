@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12"><a href="{{ redirect()->getUrlGenerator()->previous() }}"><i
                        class="fas fa-arrow-left"></i></a></div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h3>{{ __('profile.title') }}</h3>
                    <hr>
                    <form method="POST" action="{{ route('update.profile', [$user->id]) }}">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email:</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" value="{{ $user->email }}"
                                   name="email">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('profile.name') }}</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" value="{{ $user->name }}"
                                   name="name">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('profile.phone') }}</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" value="{{ $user->phone }}"
                                   name="phone">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Website:</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" value="{{ $user->website }}"
                                   name="website">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">{{ __('profile.password') }}</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">{{ __('profile.repeat') }}</label>
                            <input type="password" class="form-control" id="exampleInputPassword1"
                                   name="password_confirmation">
                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('default.save') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-0">{{ $user->name }}</h4>
                    <hr>
                    <ul>
                        <li>{{ $user->email }}</li>
                        <li>{{ $user->phone }}</li>
                        <li><a href="https://{{ $user->website }}" target="_blank">{{ $user->website }}</a></li>
                    </ul>
                    <hr>
                    <h6>{{ __('profile.recent_project') }}</h6>
                    @if (count($projects))
                        @foreach($projects->take(1)->sortBy('latest') as $project)
                            <a href="{{ route('show.project', [$project->id]) }}">
                                <div class="card card-project" style="border-top: none;">
                                    <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}"
                                         class="card-img-top" alt="{{ $project->cover_image }}">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $project->name }}</h5>
                                        <hr>
                                        <p class="card-text">{{ $project->description_short }}</p>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    @else
                        <p class="mb-0 text-center"><i class="fas fa-exclamation-triangle"></i> - You dont have any
                            projects...</p>
                    @endif
                    <hr>
                    <h6>{{ __('profile.groups') }}</h6>
                    <ul>
                        <li>Groep 1</li>
                        <li>Groep 2</li>
                        <li>Groep 3</li>
                    </ul>
                    <hr>
                    <small>{{ __('profile.joined') }} {{ Carbon\Carbon::parse($user->created_at)->format('j F Y') }} </small>
                </div>
            </div>
        </div>
    </div>

@endsection
