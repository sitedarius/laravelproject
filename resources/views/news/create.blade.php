@extends('layouts.app')

@section('title', 'Nieuws - Het Noordelijk Nieuws')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('news') }}"><i
                        class="fas fa-arrow-left"></i></a>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3>{{ __('news.create') }}</h3>
                    <hr>
                    <form method="POST" action="{{ route('store.news') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('default.title') }}*</label>
                            <input type="text"
                                   class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                   id="exampleInputEmail1" aria-describedby="emailHelp"
                                   name="title" value="{{ old('title') }}" required>
                            <p style="color: red;">{{ $errors->first('title') }}</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('default.intro') }}*</label>
                            <input type="text"
                                   class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}"
                                   id="exampleInputEmail1" aria-describedby="emailHelp"
                                   name="description" value="{{ old('description') }}" required>
                            <p style="color: red;">{{ $errors->first('description') }}</p>
                        </div>
                        <div class="form-group">
                            <label for="cover_image">{{ __('default.upload_image') }}*</label>
                            <input type="file"
                                   class="form-control-file {{ $errors->has('cover_image') ? 'is-invalid' : '' }}"
                                   id="cover_image" name="cover_image">
                            <small>{{ $errors->first('cover_image') }}</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('default.text') }}*</label>
                            <textarea class="description {{ $errors->has('body') ? 'is-invalid' : '' }}"
                                      name="body"></textarea>
                            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
                            <script>
                                tinymce.init({
                                    selector: 'textarea.description',
                                    height: 500
                                });
                            </script>
                            <p style="color: red;">{{ $errors->first('body') }}</p>
                        </div>
                        <small>* {{ __('default.required') }}</small>
                        <button type="submit" class="btn btn-primary mt-2 ml-auto">{{ __('default.save') }}</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection