@extends('layouts.app')

@section('title', 'Nieuws - Het Noordelijk Nieuws')

@section('content')

    <div class="row">
        <div class="col-md-11 col-11">
            <h3>{{ __('default.news') }}</h3>
        </div>

        <div class="col-md-1 col-1">
            @if(Auth::check() && Auth::user()->is_admin == true)
                <a href="{{ route('create.news') }}" class="btn btn-link"><i class="fas fa-plus"></i></a>
            @endif
        </div>

        <div class="col-md-12">
            <ul class="list-unstyled">
                <div class="row">
                    @foreach($posts as $post)
                        <div class="col-md-4 mt-2">
                            <div class="card">
                                <img src="/storage/news/{{ $post->id }}/{{ $post->cover_image }}">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                    <p class="card-text">{{ $post->description }} <a
                                                href="{{ route('show.news', [$post->id]) }}"
                                                class="">{{ __('news.read_more') }}</a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </ul>
        </div>
    </div>

@endsection
