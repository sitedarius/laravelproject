@extends('layouts.app')

@section('title', 'Nieuws - Het Noordelijk Nieuws')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="{{ redirect()->getUrlGenerator()->previous() }}"><i
                        class="fas fa-arrow-left"></i></a>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        @if(Auth::check() && Auth::user()->is_admin == true)
                            <div class="col-11">
                                <h3>{{ $post->title }}</h3>
                            </div>
                            <div class="col-1">
                                <a href="{{ route('edit.news', [$post->id]) }}">
                                    <small>{{ __('news.edit') }}</small>
                                </a>
                            </div>
                        @else
                            <div class="col-12">
                                <h3>{{ $post->title }}</h3>
                            </div>
                        @endif
                        <div class="col-md-12">
                            <hr>
                            <p>{!! $post->body !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <p>{{ __('news.author') }} <br>{{ __('news.by') }}</p>
                    <hr>
                    <p class="mb-0">{{ Carbon\Carbon::parse($post->created_at)->format('j F Y h:m') }}<br>
                        {{ __('news.updated') }} {{ Carbon\Carbon::parse($post->updated_at)->format('j F Y h:m') }}</p>
                </div>
            </div>
            <div class="col-12 mt-3 px-0">
                <h3>{{ __('news.comments') }}</h3>
                <form>
                    <div class="input-group">
                        <textarea class="form-control custom-control" rows="3" style="resize:none"></textarea>
                        <span class="input-group-addon btn btn-primary"><i class="fas fa-paper-plane"></i></span>
                    </div>
                </form>
                @if($comments->count())
                    @foreach($comments as $comment)
                        <div class="card mt-2">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8">
                                        <p class="mb-0"><strong>Rick</strong></p>
                                        <small>{{ Carbon\Carbon::parse($comment->created_at)->format('d-m-Y') }} <i
                                                    class="fas fa-globe"></i></small>
                                    </div>
                                    <div class="col-4 text-right">
                                            <button type="button" class="btn p-0"><i class="far fa-heart"></i></button>
                                            <button type="button" class="btn p-0"><i class="fas fa-share"></i></button>
                                            @if(Auth::user()->id == $comment->user_id)
                                            <form method="POST" action="{{ route('delete.comment', [$comment->id, $post->id]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-link p-0 delete-user"><i
                                                            class="fas fa-trash-alt"></i></button>
                                            </form>
                                            @endif
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                        <p class="mb-0">{{ $comment->comment }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p>{{ __('news.no_comments') }}</p>
                @endif
            </div>
        </div>
    </div>



@endsection
