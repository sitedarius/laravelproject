@extends('layouts.app')

@section('title', 'Admin-page - HNN')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>{{ __('Admin Page') }}</h3>
        </div>
        <div class="col-md-12 mt-3">
            @include('admin.menu')
        </div>
        @foreach($posts as $post)
            <div class="col-md-6 mt-2">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-11">
                                <h5 class="card-title">{{ $post->title }}</h5>
                            </div>
                            <div class="col-1">
                                <form method="POST" action="{{ route('delete.news', [$post->id]) }}">
                                    @csrf
                                    @method('DELETE')

                                    <div class="form-group">
                                        <button class="btn"><i class="fas fa-trash-alt"></i></button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-12">
                                <p class="card-text">{{ $post->description }} <a
                                            href="{{ route('show.news', [$post->id]) }}" class="">Read more...</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection