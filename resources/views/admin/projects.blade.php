@extends('layouts.app')

@section('title', 'Admin-page - HNN')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>{{ __('Admin Page') }}</h3>
        </div>
        <div class="col-md-12 mt-3">
            @include('admin.menu')
        </div>
        @foreach($projects as $project)
            <div class="col-md-4 mt-4">
                <a href="{{ route('show.project', [$project->id]) }}">
                    <div class="card card-project" style="border-top: none;">
                        <img src="/storage/projects/{{ $project->id }}/{{ $project->cover_image }}"
                             class="card-img-top" alt="{{ $project->cover_image }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $project->name }}</h5>
                            <hr>
                            <p class="card-text">{{ $project->description_short }}</p>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>

@endsection