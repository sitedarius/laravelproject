
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" href="{{ route('admin.index') }}">{{ __('News') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.projects') }}">{{ __('Projects') }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.users') }}">{{ __('Users') }}</a>
    </li>
</ul>