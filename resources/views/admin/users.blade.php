@extends('layouts.app')

@section('title', 'Admin-page - HNN')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h3>{{ __('Admin Page') }}</h3>
        </div>
        <div class="col-md-12 mt-3">
            @include('admin.menu')
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <button class="btn px-1"><i class="fas fa-trash-alt"></i></button>
                        <button class="btn px-1"><i class="fas fa-ban"></i></button>
                        <button class="btn px-1"><i class="fas fa-chevron-right"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection