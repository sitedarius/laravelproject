<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/project/{project}', 'ProjectController@show')->name('show.project');
Route::get('/project/create', ['middleware' => 'auth', 'uses' => 'ProjectController@create'])->name('create.project');
Route::get('/project/{project}/edit', 'ProjectController@edit')->name('edit.project');
Route::get('/projects', 'ProjectController@index')->name('projects');
Route::post('/projects', 'ProjectController@store')->name('store.project');
Route::patch('/projects/{project}/update', 'ProjectController@update')->name('update.project');
Route::delete('/projects/{project}', 'ProjectController@destroy')->name('delete.project');

Route::post('/project/{project}/tasks', 'ProjectTaskController@store')->name('store.task');
Route::patch('/tasks/{task}', 'ProjectTaskController@update')->name('update.task');
Route::delete('/tasks/{task}/delete', 'ProjectTaskController@destroy')->name('delete.task');
//
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/news/{id}', 'NewsController@show')->name('show.news');
Route::get('/create', 'NewsController@create')->name('create.news');
Route::post('/news', 'NewsController@store')->name('store.news');
Route::get('/news/{id}/edit', 'NewsController@edit')->name('edit.news');
Route::patch('/news/{id}/update', 'NewsController@update')->name('update.news');
Route::delete('/news/{id}', 'NewsController@destroy')->name('delete.news');

Route::delete('/news/{news}/comment/{comment}', 'CommentController@destroy')->name('delete.comment');

Route::post('/projects/{project}/add', 'GroupsController@store')->name('store.group');
Route::get('/projects/{project}/group/edit', 'GroupsController@edit')->name('edit.group');
Route::patch('/projects/{project}/group/{group}/edit', 'GroupsController@update')->name('update.group');

Route::get('/profile/{user}', 'ProfileController@show')->name('show.profile');
Route::patch('/profile/{user}/update', 'ProfileController@update')->name('update.profile');

Route::get('/admin', 'AdminController@index')->name('admin.index');
Route::get('/admin/users', 'AdminController@users')->name('admin.users');
Route::get('/admin/projects', 'AdminController@projects')->name('admin.projects');

Route::post('/search', 'SearchController@search')->name('search');
Route::get('/search', 'SearchController@getSearch')->name('getSearch');
